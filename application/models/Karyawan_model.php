<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model
{
	//panggil nama table
	private $_table = "karyawan";
	
	public function tampilDataKaryawan()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function rules()
	{
		return [
			[
			
				'field' => 'nik',
				'label' => 'NIK',
				'rules' => 'required|max_length[10]',
				'errors' => [
					'required' => 'NIK Tidak Boleh Kosong.',
					'max_length'=> 'NIK Tidak Boleh Lebih Dari 10 Karakter.',
				],
			],
			[
				'field' => 'nama',
				'label' => 'Nama',
				'rules' => 'required',
				'errors' => [
					'required' => 'Nama Tidak Boleh Kosong.',
				],
			],
			[
				'field' => 'tempatlahir',
				'label' => 'Tempat Lahir',
				'rules' => 'required',
				'errors' => [
					'required' => 'Tempat Lahir Tidak Boleh Kosong.',
				],
			],
			[
				'field' => 'tgl_lahir',
				'label' => 'Tanggal Lahir',
				'rules' => 'required',
				'errors' => [
					'required' => 'Tanggal Lahir Tidak Boleh Kosong.',
				],
			],
			[
				'field' => 'jenis_kelamin',
				'label' => 'Jenis Kelamin',
				'rules' => 'required',
				'errors' => [
					'required' => 'Jenis Kelamin Tidak Boleh Kosong.',
				],
			],
			[
				'field' => 'alamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => [
					'required' => 'Alamat Tidak Boleh Kosong.',
				],
			],
			[
				'field' => 'telp',
				'label' => 'Telepon',
				'rules' => 'required',
				'errors' => [
					'required' => 'Telepon Tidak Boleh Kosong.',
				],
			],
			[
				'field' => 'kode_jabatan',
				'label' => 'Jabatan',
				'rules' => 'required',
				'errors' => [
					'required' => 'Kode Jabatan Tidak Boleh Kosong.',
				],
			]
			
		];	
	}

	public function tampilDataKaryawan2()
	{
		$query = $this->db->query("SELECT * FROM karyawan WHERE flag = 1");
		return $query->result();
	
	}
	
	public function tampilDataKaryawan3()
	{
		$this->db->select('*');
		$this->db->order_by('nik', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save()
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn. "-" . $bln. "-" . $tgl;
		
		$data['nik']				=$this->input->post('nik');
		$data['nama_lengkap']		=$this->input->post('nama');
		$data['tempat_lahir']		=$this->input->post('tempatlahir');
		$data['tgl_lahir']			=$tgl_gabung;
		$data['jenis_kelamin']		=$this->input->post('jenis_kelamin');
		$data['alamat']				=$this->input->post('alamat');
		$data['telp']				=$this->input->post('telp');
		$data['kode_jabatan']		=$this->input->post('kode_jabatan');
		$data['flag']				=1;
		$this->db->insert($this->_table, $data);
	}
	
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function edit($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function update($nik)
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn. "-" . $bln. "-" . $tgl;
		
		$data['nama_lengkap']		=$this->input->post('nama');
		$data['tempat_lahir']		=$this->input->post('tempatlahir');
		$data['tgl_lahir']			=$tgl_gabung;
		$data['jenis_kelamin']		=$this->input->post('jenis_kelamin');
		$data['alamat']				=$this->input->post('alamat');
		$data['telp']				=$this->input->post('telp');
		$data['kode_jabatan']		=$this->input->post('kode_jabatan');
		$data['flag']				=1;
		
		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);
	}
	
	public function delete ($nik)
	{
		$this->db->where('nik', $nik);
		$this->db->delete($this->_table);	
	}
}
